import './onInstall'
import ipc from './ipcBackground'

const createWindow = (): Promise<any> =>
	new Promise(resolve =>
		chrome.windows.create(
			{
				incognito: true,
				focused: true
			},
			resolve
		)
	)

const navigateTab = (id: number, url: string): Promise<void> =>
	new Promise(resolve => {
		chrome.tabs.update(id, { url }, tab => {
			chrome.tabs.onUpdated.addListener(function listener(tabId, info) {
				if (info.status !== 'complete' || tabId !== tab.id) return

				chrome.tabs.onUpdated.removeListener(listener)
				resolve()
			})
		})
	})

const getScreen = (tab: any): Promise<string> =>
	new Promise(resolve =>
		chrome.desktopCapture.chooseDesktopMedia(
			['screen', 'window', 'tab'],
			tab,
			(id: string) => resolve(id)
		)
	)

ipc.on('sign-as-incognito', async (token, sender) => {
	console.log('sign-as-incognito', token, sender)
	const win = await createWindow()
	await navigateTab(win.tabs[0].id, sender.url)

	ipc.send(win.tabs[0].id, 'auth', token)
})

ipc.on('capture-screen', async (data, sender) => {
	const streamId = await getScreen(sender.tab)

	ipc.send(sender.tab.id, 'screen', streamId)
})
