const getASSTabs = (): Promise<chrome.tabs.Tab[]> =>
	new Promise(
		resolve => 
		chrome.tabs.query({
			title: `*sales.ladyagroup.ru*`
		}, tabs => resolve(tabs))
	)

const applyContentScripts = async () => {
	const tabs = await getASSTabs()

	tabs.forEach(tab => chrome.tabs.reload(tab.id))
}

chrome.runtime.onInstalled
	.addListener(details => {
		switch (details.reason) {
			case 'install':
				applyContentScripts()
				break

			case 'update':
				break
		}
	})