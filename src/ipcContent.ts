import { EventEmitter } from 'events'

interface IPCMessage {
    channel: string,
    content?: any
}

class IPC extends EventEmitter {

    constructor() {
        super()

        chrome.runtime.onMessage.addListener((msg: IPCMessage, sender) => {
			super.emit.apply(this, [msg.channel, msg.content, sender])
        })
    }

    emit(channel: string, content: any): boolean {
        window.postMessage({
            type: 'chrome-ext',
            channel,
            content
        }, window.origin)

        return true
    }
}



export default new IPC()