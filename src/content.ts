import ipc from './ipcContent'

ipc.on('auth', token => {
	localStorage.setItem('token', `__q_strn|${token}`)
	window.location.reload()
})

ipc.on('screen', id => ipc.emit('screen', id))

window.onload = function() {
	ipc.emit('register', chrome.runtime.id)
	console.log('[IPC] emit register', chrome.runtime.id)
}
