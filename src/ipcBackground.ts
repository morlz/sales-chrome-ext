import { EventEmitter } from 'events'

interface IPCMessage {
	channel: string
	content?: any
}

class IPC extends EventEmitter {
	constructor() {
		super()

		chrome.runtime.onMessageExternal.addListener(
			(msg: IPCMessage, sender) => {
				super.emit.apply(this, [msg.channel, msg.content, sender])
			},
		)
	}

	send(tabId: number | null, channel: string, content?: any): boolean {
		if (tabId === null) {
		} else {
			chrome.tabs.sendMessage(tabId, {
				channel,
				content,
			})
		}

		return true
	}
}

export default new IPC()
